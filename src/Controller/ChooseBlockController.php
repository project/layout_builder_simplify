<?php

namespace Drupal\layout_builder_simplify\Controller;

use Drupal\layout_builder\Controller\ChooseBlockController as ChooseBlockControllerCore;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines a controller to choose a new block.
 *
 * @internal
 *   Controller classes are internal.
 */
class ChooseBlockController extends ChooseBlockControllerCore {

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * ChooseBlockController constructor.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The Database service.
   */
  public function __construct(BlockManagerInterface $block_manager, EntityTypeManagerInterface $entity_type_manager, Connection $connection) {
    $this->blockManager = $block_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('entity_type.manager'),
      $container->get('database')
    );
  }

  /**
   * Provides the UI for choosing a new block.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param string $region
   *   The region the block is going in.
   *
   * @return array
   *   A render array.
   */
  public function build(SectionStorageInterface $section_storage, $delta, $region) {
    $block_categories['#type'] = 'container';
    $block_categories['#attributes']['class'][] = 'block-categories';
    $block_categories['#attributes']['class'][] = 'js-layout-builder-categories';
    $block_categories['#attributes']['data-layout-builder-target-highlight-id'] = $this->blockAddHighlightId($delta, $region);

    $delta = (int) $delta;

    $definitions = $this->blockManager->getFilteredDefinitions('layout_builder', $this->getAvailableContexts($section_storage), [
      'section_storage' => $section_storage,
      'delta' => $delta,
      'region' => $region,
    ]);
    $categories = [];
    $grouped_definitions = $this->blockManager->getGroupedDefinitions($definitions);
    foreach ($grouped_definitions as $category => $blocks) {
      $url = Url::fromRoute('layout_builder_simplify.choose_individual_block', [
        'section_storage_type' => $section_storage->getStorageType(),
        'section_storage' => $section_storage->getStorageId(),
        'delta' => $delta,
        'region' => $region,
        'category' => $category,
      ]);
      $block_categories[$category]['#type'] = 'link';
      $block_categories[$category]['#attributes'] = $this->getAjaxAttributes();
      $block_categories[$category]['#attributes']['class'][] = 'inline-block-create-button';
      $block_categories[$category]['#title'] = ucfirst($category);
      $block_categories[$category]['#url'] = $url;
    }

    $build['block_categories'] = $block_categories;
    $build['#attached']['library'][] = 'layout_builder_simplify/custom';
    return $build;
  }

  /**
   * Provides the UI for choosing a individual block as per category.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param string $region
   *   The region the block is going in.
   * @param string $category
   *   The category for which the blocks are requested.
   *
   * @return array
   *   A render array.
   */
  public function individualBlockList(SectionStorageInterface $section_storage, $delta, $region, $category) {
    $delta = (int) $delta;
    $category_class = Html::cleanCssIdentifier(strtolower($category));

    $definitions = $this->blockManager->getFilteredDefinitions('layout_builder', $this->getAvailableContexts($section_storage), [
      'section_storage' => $section_storage,
      'delta' => $delta,
      'region' => $region,
    ]);
    $grouped_definitions = $this->blockManager->getGroupedDefinitions($definitions);

    $build = [];

    $build['filter'] = [
      '#type' => 'search',
      '#title' => $this->t('Filter by block name'),
      '#title_display' => 'invisible',
      '#size' => 30,
      '#placeholder' => $this->t('Filter by block name'),
      '#attributes' => [
        'class' => [$category_class, 'js-layout-builder-filter'],
        'title' => $this->t('Enter a part of the block name to filter by.'),
      ],
    ];

    $build['back_button_top'] = [
      '#type' => 'link',
      '#url' => Url::fromRoute('layout_builder.choose_block',
        [
          'section_storage_type' => $section_storage->getStorageType(),
          'section_storage' => $section_storage->getStorageId(),
          'delta' => $delta,
          'region' => $region,
        ]
      ),
      '#title' => $this->t('Back'),
      '#attributes' => $this->getAjaxAttributes(),
    ];
    $build['back_button_top']['#attributes']['class'][] = 'button';

    $block_categories['#type'] = 'container';
    $block_categories['#attributes']['class'][] = 'block-categories';
    $block_categories['#attributes']['class'][] = 'js-layout-builder-categories';
    $block_categories['links']['#attributes']['class'][] = 'inline-block-list';

    if ($category == "Custom") {
      $build['recently_update'] = [
        '#type' => 'markup',
        '#prefix' => '<div class="form-group markup recently_updated">',
        '#suffix' => '</div>',
        '#markup' => 'Recently Updated',
      ];
      $block_categories['links'] = $this->getRecentContentBlocks($section_storage, $delta, $region, 20);

      $build['#attached']['library'][] = 'layout_builder_simplify/search';
    }
    else {
      // Build links.
      $block_categories['links'] = $this->getBlockLinks($section_storage, $delta, $region, $grouped_definitions[$category]);
    }

    $build['block_categories'] = $block_categories;

    if ($category != "Custom") {
      $build['back_button'] = [
        '#type' => 'link',
        '#url' => Url::fromRoute('layout_builder.choose_block',
          [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => $delta,
            'region' => $region,
          ]
        ),
        '#title' => $this->t('Back'),
        '#attributes' => $this->getAjaxAttributes(),
      ];
      $build['back_button']['#attributes']['class'][] = 'button';
    }

    return $build;
  }

  /**
   * Gets a render array of block links.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param string $region
   *   The region the block is going in.
   * @param array $blocks
   *   The information for each block.
   *
   * @return array
   *   The block links render array.
   */
  protected function getBlockLinks(SectionStorageInterface $section_storage, $delta, $region, array $blocks) {
    $links = [];
    foreach ($blocks as $block_id => $block) {
      $label = (isset($block['label'])) ? $block['label'] : '';
      $attributes = $this->getAjaxAttributes();
      $attributes['class'][] = 'js-layout-builder-block-link';
      $attributes['class'][] = 'inline-block-list__item';
      $link = [
        'title' => $block['admin_label'] . $label,
        'url' => Url::fromRoute('layout_builder.add_block',
          [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => $delta,
            'region' => $region,
            'plugin_id' => $block_id,
          ]
        ),
        'attributes' => $attributes,
      ];

      $links[] = $link;
    }
    return [
      '#theme' => 'links',
      '#links' => $links,
    ];
  }

  /**
   * Get dialog attributes if an ajax request.
   *
   * @return array
   *   The attributes array.
   */
  protected function getAjaxAttributes() {
    if ($this->isAjax()) {
      return [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'dialog',
        'data-dialog-renderer' => 'off_canvas',
      ];
    }
    return [];
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    $input = $request->request->get('keyword');
    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse($results);
    }
    $links = [];

    // Get all the block types.
    $types = $this->entityTypeManager->getStorage('block_content_type')->loadMultiple();

    $query = $this->connection->select('block_content', 'b');
    $query->fields('b', ['id', 'uuid', 'type']);
    $query->fields('c', ['info']);
    $query->condition('c.info', "%" . $input . "%", 'LIKE');
    $query->join('block_content_field_data', 'c', 'b.id = c.id');
    $records = $query->execute();
    foreach ($records as $record) {
      $record->type = $types[$record->type]->label();
      $links[] = $record;
    }

    if (!empty($links)) {
      foreach ($links as $link) {
        $results[] = [
          'title' => $link->info,
          'uuid' => $link->uuid,
          'type' => $link->type,
        ];
      }
    }
    else {
      $results[] = [
        'value' => NULL,
        'label' => 'No Result',
      ];
    }

    return new JsonResponse($results);
  }

  /**
   * Fetch the latest updated content blocks.
   */
  public function getRecentContentBlocks(SectionStorageInterface $section_storage, $delta, $region, $limit) {
    $results = [];
    $links = [];

    // Get all the block types.
    $types = $this->entityTypeManager->getStorage('block_content_type')->loadMultiple();

    $query = $this->connection->select('block_content', 'b');
    $query->join('block_content_field_data', 'c', 'b.id = c.id');
    $query->fields('b', ['id', 'uuid', 'type']);
    $query->fields('c', ['info', 'changed']);
    $query->orderBy('c.changed', 'DESC');
    $query->range(0, $limit);
    $records = $query->execute();

    $attributes = $this->getAjaxAttributes();
    $attributes['class'][] = 'js-layout-builder-block-link';
    $attributes['class'][] = 'inline-block-list__item';

    foreach ($records as $record) {
      $date = new \DateTime();
      $date->setTimestamp($record->changed);
      $created = ' [' . $date->format('d-m-Y H:i:s') . ']';

      $type = ' [' . $types[$record->type]->label() . ']';
      $link = [
        'title' => $record->info . $type . $created,
        'url' => Url::fromRoute('layout_builder.add_block',
          [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => $delta,
            'region' => $region,
            'plugin_id' => 'block_content:' . $record->uuid,
          ]
        ),
        'attributes' => $attributes,
      ];

      $links[] = $link;
    }
    return [
      '#theme' => 'links',
      '#links' => $links,
    ];
  }

}
