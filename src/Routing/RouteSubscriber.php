<?php

namespace Drupal\layout_builder_simplify\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('layout_builder.choose_block')) {
      $route->setDefaults([
        '_controller' => '\Drupal\layout_builder_simplify\Controller\ChooseBlockController::build',
      ]);
    }
  }

}
