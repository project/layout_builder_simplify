/**
 * @file
 * Contains layout builder simplify js.
 */
(function ($, Drupal) {
  /**
   * Implements JS to handle custom block content search.
   */
  Drupal.behaviors.searchBlocks = {
    attach: function(context) {
      'use strict';

      $('.custom.js-layout-builder-filter').flexdatalist({
        keyword: 'search',
        minLength: 1,
        searchContain: true,
        searchIn: 'title',
        url: '/block-search.json',
        cache: false,
        requestType: "POST",
        searchDelay: 200,
        noResultsText: 'The <em>{keyword}</em> return no results.'
      });

      $('.custom.js-layout-builder-filter').on('show:flexdatalist.results', function(event, result) {
        var list = '<ul class="inline-block-list links">';
        var r = localStorage.getItem('Drupal.layout_builder.region');
        var d = localStorage.getItem('Drupal.layout_builder.delta');
        var n = localStorage.getItem('Drupal.layout_builder.node');
        $.each(result, function( index, value ) {
          var link = '/layout_builder/add/block/overrides/' + n +  '/' + d + '/' + r +  '/block_content%3A' + value.uuid;
          list = list + '<li class="_"><a href="' + link + '" class="use-ajax js-layout-builder-block-link inline-block-list__item" data-dialog-type="dialog" data-dialog-renderer="off_canvas">' + value.title + ' [' + value.type + ']</a></li>';
        });
        list = list + '</ul>';
        $('.js-layout-builder-categories').html(list);
        Drupal.ajax.bindAjaxLinks(document.body);
      });

      $('.custom.js-layout-builder-filter').on('change:flexdatalist', function(event, set, options) {
        $('.recently_updated').remove();
        $('.js-layout-builder-categories').html('');
      });

      $('.custom.js-layout-builder-filter').on('shown:flexdatalist.results', function(event, result) {
        $('.flexdatalist-results').remove();
      });
    }
  };
})(jQuery, Drupal);
